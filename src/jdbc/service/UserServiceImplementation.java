package jdbc.service;

import jdbc.dao.UserDaoImplementation;
import jdbc.dao.UserDaoInterface;

import java.util.Scanner;

public class UserServiceImplementation implements UserServiceInterface{

    UserDaoInterface refInterface = new UserDaoImplementation();
    Scanner refScanner = new Scanner(System.in);

    @Override
    public void userLogin() {

        System.out.println("Enter User ID: ");
        String userID = refScanner.next();

        System.out.println("Enter password: ");
        String userPassword = refScanner.next();

        refInterface.userAuthentication(userID, userPassword);
    }
}
