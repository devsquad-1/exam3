package jdbc.dao;

import utility.DBUtility;
import java.sql.*;

public class UserDaoImplementation implements UserDaoInterface{

    Connection refConnection = DBUtility.getConnection();
    PreparedStatement refPreparedStatement = null;
    ResultSet refResult = null;

    @Override
    public void userAuthentication(String userID, String userPassword) {

        try {
            boolean userExist = false;
            String sqlQuery = "SELECT * FROM user WHERE userID=?";
            refPreparedStatement = refConnection.prepareStatement(sqlQuery);
            refPreparedStatement.setString(1, userID);
            refResult = refPreparedStatement.executeQuery();
            while (refResult.next()){
                String resultID = refResult.getString(1);
                String resultPassword = refResult.getString(2);
                if(userID.equals(resultID) && userPassword.equals(resultPassword)){
                    System.out.println("Login Successful");
                    userExist = true;
                } else {
                    throw new Exception("Login Failed! Password incorrect");
                }
            }
            if(!userExist){
                throw new Exception("invalid User ID");
            }

        } catch (SQLException e) {
            System.err.println("Error!!! Your operation is unsuccessful.");
        } // end of catch
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
