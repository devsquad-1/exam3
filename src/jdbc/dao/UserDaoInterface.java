package jdbc.dao;

public interface UserDaoInterface {
    void userAuthentication(String userID, String userPassword);
}
